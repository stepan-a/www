---
layout: page
title: "Économétrie des Séries Temporelles"
description: ""
---
{% include JB/setup %}

# Bibliographie
 - *Time Series Analysis*, James, D. Hamilton, Princeton University Press, 1994.
 - *Séries temporelles et modèles dynamiques*,  Christian Gourieroux et Alain Monfort, Economica, 1995.

# Codes
## Monte-Carlo: Biais de l'estimateur des MCO dans un modèle AR(1)
 - [simulate_ar1.m](./university/time-series/codes/monte-carlo-ar1-biais.m/simulate_ar1.m) une fonction matlab pour simuler un modèle AR(1)
 - [estimate_ar1.m](./university/time-series/codes/monte-carlo-ar1-biais.m/estimate_ar1.m) une fonction matlab pour estimer le paramètre autorégressif d'un AR(1)
 - [mc.m](./university/time-series/codes/monte-carlo-ar1-biais.m/mc.m) une fonction matlab pour estimer la distribution empirique de l'estimateur du paramètre autorégressif d'un AR(1).
 - [monte_carlo_biais.m](./university/time-series/codes/monte-carlo-ar1-biais.m/monte_carlo_biais.m) script matlab pour évaluer le biais de l'estimateur du paramètre autorégressif d'un AR(1).


# Partiels
 - 2012-2013
   Le [sujet](http://perso.univ-lemans.fr/~sadjem/cours/series-temporelles/2012-2013/partiel.pdf) et sa [correction](http://perso.univ-lemans.fr/~sadjem/cours/series-temporelles/2012-2013/correction-partiel.pdf)
