---
layout: page
title: "Enseignement"
description: ""
---
{% include JB/setup %}

Vous pouvez me contacter à l'adresse : [stephane.adjemian@univ-lemans.fr](mailto:stephane.adjemian@univ-lemans.fr). Je ne reçois pas (et *a fortiori* je ne lis pas) les mails sans sujet :
{% highlight bash %}
       if   (/^subject:[ ]+$/:h) { to /dev/null }
{% endhighlight %}
De préférence contactez-moi en utilisant votre mail de l'université (qui doit ressembler à marcel.dupont.Etu@univ-lemans.fr), autrement j'aurai plus de mal à vous répondre (si je vous réponds). Je suis généralement sur site en début de semaine ; si vous désirez me voir consultez l'emploi du temps en ligne et envoyez-moi un [mail](mailto:stephane.adjemian@univ-lemans.fr).

# Nouvelles


---
<div markdown="1" style="color:#C00000">**31 mai 2014.** Le sujet du partiel d'Économie de la concurrence du mardi 20 mai 2014 et sa correction sont en ligne [ici](./general-equilibrium-l3.html).</div>

---
<div markdown="1" style="color:#C00000">**17 mai 2014.** Le sujet du partiel d'EFC du vendredi 16 mai 2014 et sa correction sont en ligne [ici](./growth.html).</div>

---
<div markdown="1" style="color:#C00000">**13 mai 2014.** Les corrections des TD de croissance sont disponibles [ici](./growth.html).</div>


# Liens vers les cours
 - [Économétrie de la finance](./financial-econometrics.html), Master II,  Le Mans.
 - [Modèles DSGE, Simulation et estimation](./dsge-lemans.html), Master II, Le Mans.
 - [Éducation, formation et croissance](./growth.html), Licence II, Le Mans.
 - [Économie de la concurrence](./general-equilibrium-l3.html), Licence III, Le Mans.
 - [Séries temporelles](./time-series-l3.html), Licence III, Le Mans.

# Vieilleries en vraac
 - [Estimation non paramétrique de densités](./oldies/university/kernel.pdf).
 - [Moments du processus ARMA(1,1)](./oldies/university/arma11.pdf).
 - [Moments du processus ARMA(1,2)](./oldies/university/arma12.pdf).
 - [Deux examples d'économétrie bayésienne](./oldies/university/bayes_examples.pdf), [source tex](./oldies/university/bayes_examples.tex).
 - [Les moindres carrés ordinaires (I)](./oldies/university/note01.pdf)
 - [Les moindres carrés ordinaires (II)](./oldies/university/note02.pdf)
 - [Le processus AR(1)](./oldies/university/note03.pdf)
 - [Le processus AR(p)](./oldies/university/note04.pdf)
