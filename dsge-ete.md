---
layout: page
title: "Simulation et évaluation des politiques macro"
description: ""
---
{% include JB/setup %}

# Project
   Below is a list of papers for the project:
   1. Pau Rabanal et Juan F. Rubio-Ramirez (2005), *Comparing New Keynesian models of the business cycle: A bayesian approach*, in Journal of Monetary Economics, [{pdf}](./university/dsge-ete/jme-pau-2005.pdf).
   2. Thomas A. Lubik et Wing L. Teo (2010), *Inventories and optimal monetary policy*, [{pdf}](./university/dsge-ete/Lubik_Teo_072010.pdf).
   3. Thomas A. Lubik et Franck Schorfheide (2003), *Do central banks respond to exchange rate movements? A structural investigation*,[{pdf}](./university/dsge-ete/lubik_and_schorfheide,_2003.pdf).
   4. Guido Ascari et Lorenza Rossi (2009), *Trend inflation and firms price-setting: Rotemberg vs. Calvo*, [{pdf}](./university/dsge-ete/Ascari_and_Rossi,_2009.pdf).
   5. Guido Ascari, Efrem Castelnuovo et Lorenza Rossi (2010), *Calvo vs. Rotemberg in a trend inflation world: An empirical investigation*, [{pdf}](./university/dsge-ete/Ascari,_Castelnuovo_and_Rossi,_2010.pdf).
   6. Alexei Onatski and Noah Williams (2004), *Empirical and Policy Performance of a Forward Looking Monetary Model*, [{pdf}](./university/dsge-ete/Onatski_and_Williams,_2004.pdf).
   7. Marvin Goodfriend and Bennet T. McCallum (2007), *Banking and Interest Rates in Monetary Policy Analysis: A quantitative exploration*, [{pdf}](./university/dsge-ete/Goodfriend_and_McCallum,_2007.pdf).
