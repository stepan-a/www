Title: Dynare
Authors: Stéphane Adjemian
URL: dynare
save_as: dynare/index.html
Template: mainpage

I am a member of the [Dynare](http://www.dynare.org) development team since 2004. I am currently developing (in  Dynare) codes for  filtering and smoothing  with mixed frequency data, routines for non linear kalman filtering and smoothing (with Fred  Karamé), codes for the extended  path simulation approach, codes for global simulation methods.

<br>

# Links
 + [Dynare web site](www.dynare.org)
 + [Dynare wiki](www.dynare.org/DynareWiki)
 + [Dynare forum](http://www.dynare.org/phpBB3/)
 + [DSGE-net](http://www.dsge.net)
 + [Dynare sources on Github](http://github.com/DynareTeam)

<br>

# Slides
 + [Perfect foresight models](http://dynare.ithaca.fr/slides/perfect-foresight-models.pdf)
 + [Perturbation methods for DSGE models](http://dynare.ithaca.fr/slides/dsge-perturbation-method.pdf)
 + [Bayesian econometrics primer](http://dynare.ithaca.fr/slides/bayesian-econometrics-primer.pdf)
 + [Stochastic Extended Path Approach](http://www.dynare.org/stepan/slides/sep.pdf), joint with Michel Juillard
 + [Bayesian Estimation of DSGE models](http://www.dynare.org/stepan/dynare/slides/BayesianEstimation.pdf)
 + [BVAR and DSGE models](http://www.dynare.org/stepan/dynare/slides/BvarUndDsge.pdf)

<br>

# Codes

## Personal git repository
My personal [Dynare](http://www.dynare.org) [Git](http://git-scm.com/) repository is hosted by [bitbucket](http://bitbucket.org) and can be found here [here](git.ithaca.fr/dynare). If [Git](http://git-scm.com/) is installed on your computer, you can clone the repository using the following command in a shell:

	:::bash
	git clone http://git.ithaca.fr/dynare.git

This repository contains (experimental and highly unstable) additional branches (compared to [Dynare](http://www.dynare.org) official git repository). Binaries are not provided, so you have to compile the mex files and preprocessor (following the documentation given [here](http://git.ithaca.fr/dynare). There is no documentation about the new features implemented in these branches.

## Growthless Deterministic Growth Model.
This is just a small example of perfect foresight model with an analytical steadys state. The mod and steadystate file are self documented: [dog.mod](http://www.dynare.org/stepan/dynare/codes/dog/dog.mod),
[dog_steadystatestate.m](/dynare/codes/dog/dog_steadystate.m)

## Smets and Wouters (AER, 2007) like model.
The  mod and steadystate  files are provided without any documentation, but  are (should be) human (macroeconomist or dynare user) readable.  The steadystate file is called by dynare  to compute the deterministic steady  state of the non-linear model. Note that with recent version of dynare (>=4.2) it is possible to declare the analytical steady state directly in the mod file. The main limit of this new feature are that exceptions cannot be defined (for some values of the deep parameters a sensible steady state may not exist). The provided steadystate file uses persistent variables for speed improvements and defines a set of exceptions (for instance if  the depreciation rate is found to  be  negative negative  for  some values  of the  deep parameters and/or calibrated/estimated steady state ratios): [sw.mod](http://www.dynare.org/stepan/dynare/codes/sw/sw.mod), [sw_steadystate.m](http:///www.dynare.org/stepan/dynare/codes/sw/sw_steadystate.m).

<br>

# Documentation

## Priors in Dynare
   [{PDF}](http:///www.dynare.org/stepan/dynare/text/DynareDistributions.pdf) This unfinished note describes the prior densities considered in Dynare.
