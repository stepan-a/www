Title: Simulation et évaluation des politiques macro
Authors: Stéphane Adjemian
URL: teaching/dsge-p1
save_as: teaching/dsge-p1/index.html
status: hidden

<br>
<br>

### Slides

 - Bayesian econometrics primer: [pdf](http://maine.ithaca.fr/labeco/dsge/3/slides.pdf), [Git](http://git.ithaca.fr/bayesian-econometrics-primer).
 - Estimation of DSGE models: [pdf](http://maine.ithaca.fr/labeco/dsge/4/slides-1.pdf).
 
<br>
 
### Example (Estimation of a CES production function in an RBC model)
 
 <script src="https://gist.github.com/stepan-a/77ab47609722e3c3c556.js"></script>