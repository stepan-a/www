Title: Home
Authors: Stéphane Adjemian
URL: index.html
save_as: index.html
status: hidden
Template: mainpage

<br>
<br>
<br>
<br>

<center markdown="1"> Associate professor, [Department of Economics](http://ecodroit.univ-lemans.fr/), [Université du Maine](http://www.univ-lemans.fr).</center>
<center markdown="1"> stephane . adjemian AT univ - lemans . fr</center>
<center markdown="1"> Université du Maine</center>
<center markdown="1"> Faculté de Droit, des Sciences Économiques et de Gestion</center>
<center markdown="1"> Avenue Olivier Messiaen</center>
<center markdown="1"> 72085 Le Mans cedex 9</center>

<br>
<br>

<center markdown="1"> Developer in the [Dynare](http://www.dynare.org) Team.</center>
<center markdown="1"> stepan AT dynare DOT org</center>
<center markdown="1"> CEPREMAP</center>
<center markdown="1"> 142 rue Chevaleret</center>
<center markdown="1"> 75013 Paris</center>

<br>
<br>
<br>
<br>

<center markdown="1"> [My GnuPG public key](https://pgp.mit.edu/pks/lookup?op=get&search=0xA6D44CB9C64CE77B)</center>
<center markdown="1"> [http://pgp.mit.edu](http://pgp.mit.edu)</center>
<center markdown="1"> A4A0 7527 98F5 251D 239C  5AC1 A6D4 4CB9 C64C E77B</center>

<br>
<br>
<br>
<br>
<br>
<br>
