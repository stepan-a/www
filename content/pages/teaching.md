Title: Enseignement
Authors: Stéphane Adjemian
URL: teaching
save_as: teaching/index.html
Template: mainpage

Vous pouvez me contacter à l'adresse : [stephane.adjemian@univ-lemans.fr](mailto:stephane.adjemian@univ-lemans.fr). Je ne reçois pas (et *a fortiori* je ne lis pas) les mails sans sujet :

	:::bash
    if   (/^subject:[ ]+$/:h) { to /dev/null }

De préférence contactez-moi en utilisant votre e-mail de l'université (qui doit ressembler à marcel.dupont.Etu@univ-lemans.fr), autrement j'aurai plus de mal à vous répondre (si je vous réponds). Je suis généralement sur site en début de semaine ; si vous désirez me voir consultez l'emploi du temps en ligne et envoyez-moi un [mail](mailto:stephane.adjemian@univ-lemans.fr). En l'absence de réponse de ma part, n'hésitez pas à me renvoyer un e-mail.

---
<div markdown="1" style="color:#C00000">**30 janvier 2015.** La fiche de TD n°2 pour le cours de Séries temporelles est en ligne [ici](./time-series).</div>
<div markdown="1" style="color:#C00000">**28 janvier 2015.** La fiche de TD n°1 pour le cours de Séries temporelles est en ligne [ici](./time-series).</div>
<div markdown="1" style="color:#CCCCCC">**9 décembre 2014.** Le sujet d'examen et sa correction sont en ligne [ici](./economic-calculus).</div>
<div markdown="1" style="color:#CCCCCC">**4 décembre 2014.** Les sujets de CC pour le cours de Calcul économique sont en ligne [ici](./economic-calculus).</div>
<div markdown="1" style="color:#CCCCCC">**4 décembre 2014.** Correction de la fiche de TD n°4 pour le cours de Calcul économique est en ligne [ici](./economic-calculus).</div>
<div markdown="1" style="color:#CCCCCC">**25 octobre 2014.** La fiche de TD n°4 pour le cours de Calcul économique est en ligne [ici](./economic-calculus).</div>
<div markdown="1" style="color:#CCCCCC">**21 octobre 2014.** La fiche de TD n°3 pour le cours de Calcul économique est en ligne [ici](./economic-calculus).</div>
<div markdown="1" style="color:#CCCCCC">**29 septembre 2014.** La fiche de TD n°2 pour le cours de Calcul économique est en ligne [ici](./economic-calculus).</div>
<div markdown="1" style="color:#CCCCCC">**19 septembre 2014.** La fiche de TD n°1 pour le cours de Calcul économique est en ligne [ici](./economic-calculus).</div>
---

<br>

# Liens vers les cours
 - [Calcul économique](./economic-calculus), Licence I,  Le Mans.
 - [Économétrie de la finance](./financial-econometrics), Master II,  Le Mans.
 - [Éducation, formation et croissance](./growth), Licence II, Le Mans.
 - [Équilibre général](./general-equilibrium), Licence III, Le Mans.
 - [Séries temporelles](./time-series), Licence III, Le Mans.
 - [DSGE models](./dsge-p1), M2 ETE, Paris 1.
 
<br>

# Vieilleries en vraac
 - [Estimation non paramétrique de densités](../oldies/pdf/kernel.pdf).
 - [Moments du processus ARMA(1,1)](../oldies/pdf/arma11.pdf).
 - [Moments du processus ARMA(1,2)](../oldies/pdf/arma12.pdf).
 - [Deux examples d'économétrie bayésienne](../oldies/pdf/bayes_examples.pdf).
 - [Les moindres carrés ordinaires (I)](../oldies/pdf/note01.pdf)
 - [Les moindres carrés ordinaires (II)](../oldies/pdf/note02.pdf)
 - [Le processus AR(1)](../oldies/pdf/note03.pdf)
 - [Le processus AR(p)](../oldies/pdf/note04.pdf)
