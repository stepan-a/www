Title: Thèse
Authors: Stéphane Adjemian
URL: research/thesis
save_as: research/thesis/index.html
status: hidden

*Divergence des  Nations & Progrès Technique*, Thèse  sous la direction de [Jérôme Glachant](http://sites.google.com/site/jeromeglachant/) soutenue en Décembre 2002 à [l'université d'Évry](http://www.univ-evry.fr/).
 - Thèse au format [pdf](http://www.dynare.org/stepan/thesis/thesis.pdf).
 - Thèse au format [postcript](http://www.dynare.org/stepan/thesis/thesis.ps).
 - Toutes les sources dans une [archive tar.bz2](http://www.dynare.org/stepan/thesis/thesis.tar.bz2).

Pour recompiler la thèse à partir des sources

	:::bash
	$ mkdir tmp
	$ mv thesis.tar.bz2 tmp
	$ cd tmp
	$ tar xjvf thesis.tar.bz2

puis (pour obtenir un fichier postcript) :

	:::bash
	$ latex Master
	$ bibtex Master
	$ latex Master
	$ latex Master
	$ dvips Master.dvi

ou (pour obtenir un fichier pdf) :

	:::bash
	$ pdflatex Master
	$ bibtex Master
	$ pdflatex Master
	$ pdflatex Master
