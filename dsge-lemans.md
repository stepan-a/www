---
layout: page
title: "Modèles DSGE."
description: ""
---
{% include JB/setup %}

Ce cours de vingt heures traite de la résolution et de l'estimation des modèles DSGE (*Dynamic Stochastic General Equilibrium*). Nous présentons plusieurs stratégies de simulation et considérons l'estimation de ces modèles par maximum de vraisemblance ou par l'approche Bayésienne. Le cours est sanctionné par la réalisation d'un projet.

# Bibliographie
- *The ABCs of RBCs: An Introduction to Dynamic Macroeconomic Models*, George McCandless, Harvard University Press, 2008. 
- *Structural Macroconometrics*,   David N. DeJong et Chetan Dave, Princeton University Press, 2011.

# Ressources
 - [Octave](http://www.gnu.org/software/octave/), un clone libre de Matlab.
 - [Julia](http://julialang.org), un nouveau lanqage scientifique très prometteur.
 - [Dynare](http://www.dynare.org), une librairie Matlab, compatible avec Octave, facilitant la simulation et l'estimation des modèles DSGE.
 - [Iris](https://code.google.com/p/iris-toolbox-project/), une autre librarie Matlab pour simuler et estimer des modèles DSGE (non compatible avec Octave).
 - [A toolkit for analyzing nonlinear economic dynamic models easily](http://www2.wiwi.hu-berlin.de/institute/wpol/html/toolkit.htm), par Harald Uhlig.
 
# Notes de cours
 - Modèle RBC canonique, [{pdf}](./university/dsge/rbc-basic/slides.pdf).

# Modèle RBC avec dynare (version à anticipations parfaites).
{% gist 6974512 %}

# Projet
Pour le projet je vous propose de travailler sur ce [papier](./university/dsge-ete/ireland_prod.pdf) de Peter Ireland. Le but est de tenter de reproduire les résultats présentés dans le papier. Pour vous aider, vous pouvez profiter des codes (et données) fournis par l'auteur sur cette [page](https://www2.bc.edu/peter-ireland/programs.html). Peter Ireland n'utilise pas [Dynare](http://www.dynare.org), mais un regard sur ces codes (matlab) peut être utile. La première étape, pour vous, est de défricher le modèle, de le coder en [Dynare](http://www.dynare.org) et de vérifier qu'il n'y a pas de problème (par exemple en comparant les IRFs). Dans un second temps vous pourrez vous lancer dans l'estimation du modèle. Vous serez sûrement confrontés à de nombreux problèmes n'hésitez pas à me contacter par e-mail, on peut aussi se voir au Mans. L'évaluation sera basée sur une présentation orale de votre travail et sera individuelle.
