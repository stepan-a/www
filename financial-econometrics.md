---
layout: page
title: "Économétrie financière"
description: ""
---
{% include JB/setup %}

Ce cours de séries temporelles a pour objectif de familiariser les étudiants avec les modèles spécifiques à la finance (modèles ARCH, GARCH, ...). On commence par revenir sur les processus stochastiques linéaires gaussiens avec les modèles VAR, en expliquant pourquoi ces modèles ne sont pas aptes à rendre compte des séries financières. On introduit ensuite des non linearités en montrant comment celles-ci aident à modéliser les séries financières. Nous étudions les propriétés des modèles (G)ARCH, montrons comment les estimer par maximum de vraisemblance, puis abordons le problème de la sélection de modèle et de la prévision.   

# Bibliographie
 - *Modèles ARCH  et applications financières*, Christian Gourieroux, Economica, 1992.
 - *Modèles GARCH, structure, inférence statistique et applications financières*, Christian Francq et Jean-Michel Zakoian, Economica, 2009.
 - *New Introduction to Multiple Time Series Analysis*, Helmut Lütkpohl, Springer, 2005.

# Ressources
 - [Octave](http://www.gnu.org/software/octave/), un clone libre de Matlab.
 - [R](http://www.r-project.org/), un clone libre de S. Ce logiciel dispose de nombreuses librairies, voir plus bas.
 - [Julia](http://julialang.org), un nouveau lanqage scientifique très prometteur.
 - La [documentation de R](http://cran.r-project.org/manuals.html) sur le site officiel.
 - De la [documentation](http://cran.r-project.org/other-docs.html) sur [R](http://www.r-project.org/)  et  ses  libraries écrite par des utilisateurs.
 - Un [moteur de recherche pour R](http://finzi.psych.upenn.edu/search.html).
 - Un [forum](http://r.789695.n4.nabble.com/) d'utilisateurs de [R](http://www.r-project.org/), où  vous pourrez  trouver  des informations,  voire poser  des questions (mais consultez les docuumentations au préalable).
 - Pour toute question, n'hésitez pas à utiliser le [forum](https://maine.ithaca.fr/forums) dédié à ce cours. Il faut au préalable vous inscrire, la démarche à suivre est décrite [ici](./forums.html). 

# Projet
  Le projet qui sanctionnera ce cours est disponible [ici](http://perso.univ-lemans.fr/~sadjem/cours/methodes-econometriques-pour-la-finance/2013-2014/devoir.pdf) avec la source [tex](http://perso.univ-lemans.fr/~sadjem/cours/methodes-econometriques-pour-la-finance/2013-2014/devoir.tex). Je suis disponible jusqu'aiu mois de juillet pour répondre aux éventuelles questions relatives au projet. Il faudra rendre votre travail sous la forme d'un fichier pdf (je ne lis pas les documents word) par e-mail à mon adresse [stephane.adjemian@univ-lemans.fr](mailto:stephane.adjemian@univ-lemans.fr).
