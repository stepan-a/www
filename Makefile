ROOT_PATH = .

growth:
	$(MAKE) -C $(ROOT_PATH)/university/growth all

pac:
	$(MAKE) -C $(ROOT_PATH)/university/pac all

time-series:
	$(MAKE) -C $(ROOT_PATH)/university/time-series all

dsge:
	$(MAKE) -C $(ROOT_PATH)/university/dsge/rbc-basic all

ge:
	$(MAKE) -C $(ROOT_PATH)/university/ge all

all: dsge growth ge
