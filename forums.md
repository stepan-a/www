---
layout: page
title: "Forums"
description: ""
---
{% include JB/setup %}

/!\ Les forums sont en ce moment hors ligne /!\

Pour certains cours (*Méthodes économétriques pour la finance *, *Éducation, Formation  et Croissance* et *Économie de la concurrence*) vous pouvez poser des questions et discuter sur les forums disponibles à l'adresse [https://maine.ithaca.fr/forums](https://maine.ithaca.fr/forums). Comme le signale le protocole utilisé (https), les communications vers ce site sont cryptées afin de protéger le mot de passe qu'il vous faudra définir. Lorsque vous suivrez ce lien votre navigateur vous signalera qu'il y a un problème de confiance. Vous pouvez soit passer l'avertissement soit, avec certains navigateurs, passer l'avertissement en ajoutant une exception afin de ne pas retrouver cet avertissement à chaque visite sur le site. Si vous ajoutez une exception vérifiez que la *fingerprint* du certificat ssl correspond bien à [celle-ci](./maine.ithaca.fr-fp) de façon à être certain que vous vous connectez sur le bon serveur. Une fois sur la page, vous devez suivre les étapes suivantes lors de la première visite pour créer un compte :

1. Clique sur inscription (en haut à droite).
2. Accepter les conditions d'inscription.
3. Choisir un nom d'utilisateur. Par exemple : StéphaneAdjemian (c'est déjà pris ;-).
4. Donner votre adresse e-mail (celle de l'université).
5. Définir un mot de passe.
6. Choisir la langue, vous avez le choix entre le français et l'anglais.
7. Écrire le code de confirmation.
8. Appuyer sur <<Envoyer>> au bas de la page.

<br>

Vous recevrez immédiatement un e-mail contenant un lien sur lequel il faut cliquer pour activer votre compte. Il ne vous reste plus qu'à vous connecter, choisir le forum, cliquer sur NOUVEAUSUJET pour rédiger une question. Évidemment vous pouvez aussi répondre aux questions de vos collègues ou à vos questions. Avant de poser une question, prenez soin de vérifier que votre problème n'a pas déjà été abordé et résolu. Dorénavant, je ne répondrai plus aux e-mails abordant des questions de cours/td, il vous faudra utiliser les forums!
