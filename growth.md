---
layout: page
title: "Éducation, Formation et Croissance"
description: ""
---
{% include JB/setup %}

# Bibliographie
 - *La croissance économique*, Robert Barro et Xavier Sala-i-Martin, Delmas et Cie, 1996.
 - *Théories de la croissance endogène*, Charles I. Jones, De Boeck Université, 1999.
 - *A contribution to the empirics of economic growth*, N. Gregory Mankiw, David Romer et David N. Weil, in *The Quarterly Journal of Economics*, Vol. 107, No. 2, pp. 407-437, 1992. [pdf](http://www.jstor.org/discover/10.2307/2118477?uid=3738016&uid=2&uid=4&sid=21103387773843) sur jstor.org.

# Ressources
 - [Penn World Tables](http://www.rug.nl/research/ggdc/data/penn-world-table) Une base de données sur la croissance dans le monde après la seconde guerre mondiale.
 - [Barro-Lee dataset of educational attainment](http://www.barrolee.com/data/dataexp.htm) Une basse de données décrivant les niveaux d'éducation dans le monde de 1950 à 2010.

# Travaux dirigés
 - Fiche de TD n°1 : [pdf](./university/growth/td/1/td1.pdf), [tex](./university/growth/td/1/td1.tex), [correction](./university/growth/td/1/correction-td1.pdf).
 - Fiche de TD n°2 : [pdf](./university/growth/td/2/td2.pdf), [tex](./university/growth/td/2/td2.tex), [correction](./university/growth/td/2/correction-td2.pdf).
 - Fiche de TD n°3 : [pdf](./university/growth/td/3/td3.pdf), [tex](./university/growth/td/3/td3.tex), [correction](./university/growth/td/3/correction-td3.pdf).

# Partiel
 - Le [sujet](./university/growth/ds/partiel-2014.pdf) et sa [correction](./university/growth/ds/correction-partiel-2014.pdf).
