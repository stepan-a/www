#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

LOAD_CONTENT_CACHE = False

AUTHOR = u'Stéphane Adjemian'
SITENAME = u'Stéphane Adjemian'
SITEURL = 'www.dynare.org/stepan'

PATH = '/home/stepan/www/content/'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

#Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('GitHub', 'https://github.com/stepan-a'),
          ('Bitbucket', 'https://bitbucket.org/stepan-a'),)

DEFAULT_PAGINATION = 10

# static paths will be copied without parsing their contents
STATIC_PATHS = [ "oldies/", "slides/", "university/", "dynare-files/", "favicon.ico", "img/", "pdf/", "data/"]
PAGE_EXCLUDES = [ "content/university/time-series/codes/monte-carlo-ar1-biais.m/LICENSE.md", ]

FAVICON='favicon.ico'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME="./themes/pelican-bootstrap3"

CC_LICENSE = "CC-BY-SA"
