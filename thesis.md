---
layout: page
title: "Thesis"
description: ""
---
{% include JB/setup %}

*Divergence des  Nations & Progrès Technique*, Thèse  sous la direction de [Jérôme Glachant](http://sites.google.com/site/jeromeglachant/) soutenue en Décembre 2002 à [l'université d'Évry](http://www.univ-evry.fr/).
 - Thèse au format [pdf](http://www.dynare.org/stepan/thesis/thesis.pdf).
 - Thèse au format [postcript](http://www.dynare.org/stepan/thesis/thesis.ps).
 - Toutes les sources dans une [archive tar.bz2](http://www.dynare.org/stepan/thesis/thesis.tar.bz2).

Pour recompiler la thèse à partir des sources
{% highlight bash %}
$ mkdir tmp
$ mv thesis.tar.bz2 tmp
$ cd tmp
$ tar xjvf thesis.tar.bz2
{% endhighlight %}
puis (pour obtenir un fichier postcript) :
{% highlight bash %}
$ latex Master
$ bibtex Master
$ latex Master
$ latex Master
{% endhighlight %}
$ dvips Master.dvi
ou (pour obtenir un fichier pdf) :
{% highlight bash %}
$ pdflatex Master
$ bibtex Master
$ pdflatex Master
$ pdflatex Master
{% endhighlight %}
