---
layout: page
title: "Économie de la concurrence"
description: ""
---
{% include JB/setup %}

# Bibliographie
 - *Introduction à la microéconomie*, Hal, R. Varian, De Boeck, 2011.
 - *Analyse microéconomique*, Hal, R. Varian, De Boeck, 2008.

# Travaux dirigés
 - Fiche de TD n°1 : [pdf](./university/ge/td/1/td1.pdf), [tex](./university/ge/td/1/td1.tex).
 - Fiche de TD n°2 : [pdf](./university/ge/td/2/td2.pdf), [tex](./university/ge/td/2/td2.tex).
 - Fiche de TD n°3 : [pdf](http://perso.univ-lemans.fr/~sadjem/cours/economie-de-la-concurrence/td3.pdf).

# Partiel
 - Le [sujet](./university/ge/ds/partiel-2014.pdf) et sa [correction](./university/ge/ds/correction-partiel-2014.pdf).
