---
title: Stéphane Adjemian
layout: page
---
{% include JB/setup %}

<center markdown="1"> Associate professor, [Department of Economics](http://ecodroit.univ-lemans.fr/), [Université du Maine](http://www.univ-lemans.fr).</center>
<center markdown="1"> stephane . adjemian AT univ - lemans . fr</center>
<center markdown="1"> Université du Maine</center>
<center markdown="1"> Faculté de Droit, des Sciences Économiques et de Gestion</center>
<center markdown="1"> Avenue Olivier Messiaen</center>
<center markdown="1"> 72085 Le Mans cedex 9</center>

<br>
<br>

<center markdown="1"> Developer in the [Dynare](http://www.dynare.org) Team.</center>
<center markdown="1"> stepan AT dynare DOT org</center>
<center markdown="1"> CEPREMAP</center>
<center markdown="1"> 142 rue Chevaleret</center>
<center markdown="1"> 75013 Paris</center>

<br>
<br>
<br>
<br>

<center markdown="1"> [My GnuPG public key](./stepan.gpg-pub.asc)</center>
<center markdown="1"> [http://pgp.mit.edu](http://pgp.mit.edu)</center>
<center markdown="1"> DDAE 7969 7FC4 5EF5 11D6  CDFD AD03 9A78 4539 0197</center>
