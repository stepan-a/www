---
layout: page
title: "Research"
description: ""
---
{% include JB/setup %}

## Papers
 - *Stochastic Extended Path Simulation Method*, joint with Michel Juillard [{PDF Slides}](http://www.dynare.org/stepan/slides/sep.pdf)
- *Accuracy of the Extended Path Simulation Method in a New Keynesian Model with Zero Lower Bound on the Nominal Interest Rate*, joint with Michel Juillard. [{PDF}](http://www.dynare.org/stepan/papers/ZLB-ExtendedPath-Accuracy.pdf).
 - *Évaluation de la politique monétaire dans un modèle DSGE pour la zone euro*, joint with Antoine Devulder. [{PDF}](http://www.dynare.org/stepan/papers/rfe-2011-a.pdf) and [{tar.bz2 archive}](http://www.dynare.org/stepan/papers/rfe-2011.tar.bz2) containing the files needed to get the results reported in the paper.
 - *Optimal monetary policy and the transmission of oil-supply shocks to the euro area under rational expectations*, joint with Matthieu Darracq Pariès. [ECB Working Paper version](http://www.ecb.eu/pub/pdf/scpwps/ecbwp962.pdf), 2008. 
 - *Towards a  monetary policy evaluation  framework*, joint with Stéphane Moyen and Matthieu Darracq Pariès. [ECB Working Paper version](http://www.ecb.eu/pub/pdf/scpwps/ecbwp942.pdf), 2008.
 - *A Quantitative Perspective on Optimal Monetary Policy Cooperation between the US and the Euro Area*, joint with Matthieu Darracq Pariès and Franck Smets. [ECB Working Paper version](http://www.ecb.int/pub/pdf/scpwps/ecbwp884.pdf), 2008.
 - *Un regard bayésien sur les modèles dynamiques de la macroéconomie*, joint with Florian Pélegrin. [{PDF}](http://www.dynare.org/stepan/papers/ecoprev2007b.pdf) published in Économie et Prévision, 2008.
 - *Variantes en univers incertain*, joint with Christophe Cahn, Antoine Devulder and Nicolas Maggiar. [{PDF}](http://www.dynare.org/stepan/papers/ecoprev2007a.pdf) published in Économie et Prévision, 2008.
 - *Assessing the International Spillovers between the US and the Euro Area: evidence from a two country DSGE -- VAR*, joint with Matthieu Darracq Pariès. [{PDF}]([[http://www.dynare.org/stepan/papers/ad2007.pdf) version, 2007.
 - *Optimal Monetary Policy in an Estimated DSGE for the Euro Area*, joint with Stéphane Moyen and Matthieu Darracq Pariès. [{PDF}](http://www.dynare.org/stepan/papers/adm2007.pdf) or [ECB Working Paper version](http://www.ecb.eu/pub/pdf/scpwps/ecbwp803.pdf), 2007.
 - *Shumpeterian Growth, Unemployment and the labor market institutions*, joint with François Langot and Coralia Quintero-Rojas. [{PDF}](http://mpra.ub.uni-muenchen.de/7909/2/MPRA_paper_7909.pdf), 2007.
 - *Convergence des productivités européennes.  Transition, rupture   et  racine   unitaire*, [{PDF}](http://www.pse.ens.fr/adres/anciens/n69/vol69-02.pdf) published  in   Annales d'Économie et de Statistiques, 2003.
 - *Cible  d'inflation ou  de niveau  de prix :  quelle option retenir pour la banque  centrale dans un environnement Nouveau Keynésien*, [{PDF}](http://www.cairn.info/load_pdf.php?ID_ARTICLE=REL_693_0293) published in Louvain Economic Review, 2003.
 
## Thesis
 Ma thèse est disponible [ici](./thesis.html).
 
## Codes
 
